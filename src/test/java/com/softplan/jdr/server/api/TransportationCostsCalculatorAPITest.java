package com.softplan.jdr.server.api;

import com.softplan.jdr.server.TestServer;
import com.softplan.jdr.server.model.Vehicle;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

public class TransportationCostsCalculatorAPITest implements TransportationCostsCalculatorAPI.Constants {

    private static TestServer server;
    private static Client client;
    private static Vehicle[] savedVehicles;

    @BeforeClass
    public static void globalSetUp() throws Exception {
        server = TestServer.wrap(TransportationCostsCalculatorAPI.class);
        client = ClientBuilder.newClient();

        final ObjectMapper mapper = new ObjectMapper();
        savedVehicles = mapper.readValue(ClassLoader.getSystemResourceAsStream("vehicles.json"), Vehicle[].class);
    }

    @AfterClass
    public static void globalTearDown() {
        server.stop();
        client.close();
    }

    private static WebTarget target(final String path) {
        return client.target(server.baseUri() + path);
    }

    @Test
    public void listAvailableVehicles() {
        final Response response = target(CONTEXT + VEHICLES)
                .request(MediaType.APPLICATION_JSON)
                .get();

        Assert.assertEquals(200, response.getStatus());
        final List<Vehicle> result = response.readEntity(new GenericType<List<Vehicle>>() {});

        Assert.assertArrayEquals(savedVehicles, result.toArray());
    }

    @Test
    public void calculateCostOfTransportation() {
        final Response response = target(CONTEXT + CALCULATE)
                .queryParam(Params.VEHICLE, 2)
                .queryParam(Params.WEIGHT, 8)
                .queryParam(Params.PAVED, 50)
                .queryParam(Params.UNPAVED, 50)
                .request(MediaType.TEXT_PLAIN)
                .get();

        Assert.assertEquals(200, response.getStatus());
        final Double cost = response.readEntity(Double.class);

        Assert.assertEquals(66.90, cost, 0);
    }

    @Test
    public void calculateCostOfTransportationWithInvalidVehicleShouldReturnError() {
        final Response response = target(CONTEXT + CALCULATE)
                .queryParam(Params.VEHICLE, 4)
                .queryParam(Params.WEIGHT, 8)
                .queryParam(Params.PAVED, 50)
                .queryParam(Params.UNPAVED, 50)
                .request(MediaType.TEXT_PLAIN)
                .get();

        Assert.assertEquals(400, response.getStatus());
    }

    @Test
    public void calculateCostOfTransportationWithInvalidWeightShouldReturnError() {
        final Response response = target(CONTEXT + CALCULATE)
                .queryParam(Params.VEHICLE, 1)
                .queryParam(Params.WEIGHT, -1)
                .queryParam(Params.PAVED, 50)
                .queryParam(Params.UNPAVED, 50)
                .request(MediaType.TEXT_PLAIN)
                .get();

        Assert.assertEquals(400, response.getStatus());
    }

    @Test
    public void calculateCostOfTransportationWithInvalidPavedDistanceShouldReturnError() {
        final Response response = target(CONTEXT + CALCULATE)
                .queryParam(Params.VEHICLE, 1)
                .queryParam(Params.WEIGHT, 5)
                .queryParam(Params.PAVED, -1)
                .queryParam(Params.UNPAVED, 50)
                .request(MediaType.TEXT_PLAIN)
                .get();

        Assert.assertEquals(400, response.getStatus());
    }

    @Test
    public void calculateCostOfTransportationWithInvalidUnpavedDistanceShouldReturnError() {
        final Response response = target(CONTEXT + CALCULATE)
                .queryParam(Params.VEHICLE, 1)
                .queryParam(Params.WEIGHT, 5)
                .queryParam(Params.PAVED, 50)
                .queryParam(Params.UNPAVED, -1)
                .request(MediaType.TEXT_PLAIN)
                .get();

        Assert.assertEquals(400, response.getStatus());
    }
}
