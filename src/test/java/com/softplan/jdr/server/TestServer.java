package com.softplan.jdr.server;

import io.undertow.servlet.api.DeploymentInfo;

import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

public class TestServer {

    public static class TestApplication extends Application {

        private static final Set<Class<?>> resources = new HashSet<>();
        public static void add(final Class<?> apiClass) {
            resources.add(apiClass);
        }

        public static void clear() {
            resources.clear();
        }

        @Override
        public Set<Class<?>> getClasses() {
            return resources;
        }

    }
    private final Server server;

    private TestServer() {
        server = new Server(8080, "127.0.0.1");
    }

    private <T extends Application> void deploy(final Class<T> appClass) {
        final DeploymentInfo di = server.deployApplication("/test", appClass)
                .setClassLoader(TestServer.class.getClassLoader())
                .setContextPath("/")
                .setDeploymentName("TestServer");
        server.deploy(di);
    }

    public static TestServer wrap(final Class<?> ... apis) {
        final TestServer ts = new TestServer();
        TestApplication.clear();
        for (final Class<?> api : apis) {
            TestApplication.add(api);
        }
        ts.deploy(TestApplication.class);
        return ts;
    }

    public void stop() {
        server.server.stop();
    }

    public String baseUri() {
        return "http://127.0.0.1:8080/test";
    }
}
