package com.softplan.jdr.core;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class TransportationCostsCalculatorTest {

    private TransportationCostsCalculator tcc;

    @Before
    public void setUp() {
        tcc = new TransportationCostsCalculator();
    }

    @Test
    public void zeroDistanceShouldReturnZeroCost() {
        final double cost = tcc.calculate(0);
        Assert.assertEquals(0.0, cost, 0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void negativeWeightShouldThrowException() {
        tcc.calculate(-1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void negativeFactorShouldThrowException() {
        tcc.calculate(0, -1);
    }

    @Test(expected = Test.None.class)
    public void shouldBeAbleToSpecifyDistanceByRoadType() {
        tcc.add(0, RoadType.PAVED);
    }

    @Test(expected = IllegalArgumentException.class)
    public void negativeDistanceShouldThrowException() {
        tcc.add(-1, RoadType.PAVED);
    }

    @Test(expected = IllegalArgumentException.class)
    public void nullRoadTypeShouldThrowException() {
        tcc.add(0, null);
    }

    @Test(expected = Test.None.class)
    public void shouldBeAbleToSetDistanceByRoadType() {
        tcc.set(0, RoadType.PAVED);
    }

    @Test(expected = IllegalArgumentException.class)
    public void setNegativeDistanceShouldThrowException() {
        tcc.set(-1, RoadType.PAVED);
    }

    @Test(expected = IllegalArgumentException.class)
    public void setNullRoadTypeShouldThrowException() {
        tcc.set(0, null);
    }


    @Test(expected = Test.None.class)
    public void shouldBeAbleToConfigureCostByRoadType() {
        tcc.setCost(0, RoadType.PAVED);
    }

    @Test(expected = IllegalArgumentException.class)
    public void configureNegativeCostShouldThrowException() {
        tcc.setCost(-1, RoadType.PAVED);
    }

    @Test(expected = IllegalArgumentException.class)
    public void configureNullRoadTypeShouldThrowException() {
        tcc.setCost(0, null);
    }

    @Test(expected = Test.None.class)
    public void shouldBeAbleToSpecifyWeightThreshold() {
        tcc.setWeightThreshold(0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void negativeWeightThresholdShouldThrowException() {
        tcc.setWeightThreshold(-1);
    }

    @Test(expected = Test.None.class)
    public void shouldBeAbleToSpecifyExceedingWeightCost() {
        tcc.setExceedingWeightCost(0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void negativeExceedingWeightCostShouldThrowException() {
        tcc.setExceedingWeightCost(-1);
    }

    @Test
    public void calculateCostWithOneTypeOfRoad() {
        tcc.setCost(0.54, RoadType.PAVED);
        tcc.add(100, RoadType.PAVED);
        final double cost = tcc.calculate(0);
        Assert.assertEquals(54, cost, 0);
    }

    @Test
    public void calculateCostWithMoreThanOneTypeOfRoad() {
        tcc.setCost(0.54, RoadType.PAVED);
        tcc.setCost(0.62, RoadType.UNPAVED);

        tcc.add(100, RoadType.PAVED);
        tcc.add(100, RoadType.UNPAVED);

        final double cost = tcc.calculate(0);
        Assert.assertEquals(116, cost, 0);
    }

    @Test
    public void exceedingWeightShouldIncreaseCostByDistance() {
        tcc.setCost(1, RoadType.PAVED);
        tcc.setWeightThreshold(1);
        tcc.setExceedingWeightCost(0.5);

        tcc.add(2, RoadType.PAVED);
        double cost = tcc.calculate(1);
        Assert.assertEquals(2, cost, 0);

        cost = tcc.calculate(4);
        Assert.assertEquals(5, cost, 0);

        tcc.add(1, RoadType.PAVED);

        cost = tcc.calculate(4);
        Assert.assertEquals(7.5, cost, 0);
    }

    @Test
    public void factorShouldAffectFinalValue() {
        tcc.setCost(0.54, RoadType.PAVED);
        tcc.add(100, RoadType.PAVED);

        double cost = tcc.calculate(0);
        Assert.assertEquals(54, cost, 0);

        cost = tcc.calculate(0, 0.5);
        Assert.assertEquals(27, cost, 0);

        cost = tcc.calculate(0, 1.5);
        Assert.assertEquals(81, cost, 0);
    }

}
