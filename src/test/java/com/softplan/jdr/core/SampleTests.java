package com.softplan.jdr.core;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class SampleTests {

    private TransportationCostsCalculator tcc;

    @Before
    public void setUp() {
        tcc = new TransportationCostsCalculator();
        tcc.setCost(0.54, RoadType.PAVED);
        tcc.setCost(0.62, RoadType.UNPAVED);
        tcc.setWeightThreshold(5);
        tcc.setExceedingWeightCost(0.02);
    }

    @Test
    public void sampleTest1() {
        tcc.add(100, RoadType.PAVED);
        tcc.add(0, RoadType.UNPAVED);

        double cost = tcc.calculate(8, 1.05);
        Assert.assertEquals(62.7, cost, 0);
    }

    @Test
    public void sampleTest2() {
        tcc.add(0, RoadType.PAVED);
        tcc.add(60, RoadType.UNPAVED);

        double cost = tcc.calculate(4, 1.00);
        Assert.assertEquals(37.20, cost, 0);
    }

    @Test
    public void sampleTest3() {
        tcc.add(0, RoadType.PAVED);
        tcc.add(180, RoadType.UNPAVED);

        double cost = tcc.calculate(12, 1.12);
        Assert.assertEquals(150.19, cost, 0);
    }

    @Test
    public void sampleTest4() {
        tcc.add(80, RoadType.PAVED);
        tcc.add(20, RoadType.UNPAVED);

        double cost = tcc.calculate(6, 1.00);
        Assert.assertEquals(57.60, cost, 0);
    }

    @Test
    public void sampleTest5() {
        tcc.add(50, RoadType.PAVED);
        tcc.add(30, RoadType.UNPAVED);

        double cost = tcc.calculate(5, 1.05);
        Assert.assertEquals(47.88, cost, 0);
    }
}
