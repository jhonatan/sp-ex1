function request(method, url) {
    return new Promise((resolve, reject) => {
        const xhr = new XMLHttpRequest();
        xhr.open(method, url);
        xhr.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                resolve(this.responseText);
            } else if (this.status !== 200) {
                reject(this);
            }
        };
        xhr.send();
    });
}

Vue.use(VeeValidate, {
    locale: 'pt_BR',
    dictionary: {
        pt_BR: {
            attributes: {
                vehicle: 'Veículo',
                weight: 'Carga',
                paved: 'Distância em rodovia pavimentada',
                unpaved: 'Distância em rodovia não-pavimentada',
            },
            messages: {
                _default: (field) => `O valor do campo ${field} não é válido.`,
                after: (field, [target]) => `O campo ${field} deve estar depois do campo ${target}.`,
                alpha_dash: (field) => `O campo ${field} deve conter letras, números e traços.`,
                alpha_num: (field) => `O campo ${field} deve conter somente letras e números.`,
                alpha_spaces: (field) => `O campo ${field} só pode conter caracteres alfabéticos e espaços.`,
                alpha: (field) => `O campo ${field} deve conter somente letras.`,
                before: (field, [target]) => `O campo ${field} deve estar antes do campo ${target}.`,
                between: (field, [min, max]) => `O campo ${field} deve estar entre ${min} e ${max}.`,
                confirmed: (field, [confirmedField]) => `Os campos ${field} e ${confirmedField} devem ser iguais.`,
                credit_card: (field) => `O campo ${field} é inválido.`,
                date_between: (field, [min, max]) => `O campo ${field} deve estar entre ${min} e ${max}.`,
                date_format: (field, [format]) => `O campo ${field} deve estar no formato ${format}.`,
                decimal: (field, [decimals = '*'] = []) => `O campo ${field} deve ser numérico e deve conter ${!decimals || decimals === '*' ? '' : decimals} casas decimais.`,
                digits: (field, [length]) => `O campo ${field} deve ser numérico e ter exatamente ${length} dígitos.`,
                dimensions: (field, [width, height]) => `O campo ${field} deve ter ${width} pixels de largura por ${height} pixels de altura.`,
                email: (field) => `O campo ${field} deve ser um email válido.`,
                ext: (field) => `O campo ${field} deve ser um arquivo válido.`,
                image: (field) => `O campo ${field} deve ser uma imagem.`,
                included: (field) => `O campo ${field} deve ter um valor válido.`,
                integer: (field) => `O campo ${field} deve ser um número inteiro.`,
                ip: (field) => `O campo ${field} deve ser um endereço IP válido.`,
                length: (field, [length, max]) => {
                    if (max) {
                        return `O tamanho do campo ${field} está entre ${length} e ${max}.`;
                    }

                    return `O tamanho do campo ${field} deve ser ${length}.`;
                },
                max: (field, [length]) => `O campo ${field} não deve ter mais que ${length} caracteres.`,
                max_value: (field, [max]) => `O campo ${field} precisa ser ${max} ou menor.`,
                mimes: (field) => `O campo ${field} deve ser um tipo de arquivo válido.`,
                min: (field, [length]) => `O campo ${field} deve conter pelo menos ${length} caracteres.`,
                min_value: (field, [min]) => `O campo ${field} precisa ser ${min} ou maior.`,
                excluded: (field) => `O campo ${field} deve ser um valor válido.`,
                numeric: (field) => `O campo ${field} deve conter apenas números`,
                regex: (field) => `O campo ${field} possui um formato inválido.`,
                required: (field) => `O campo ${field} é obrigatório.`,
                size: (field, [size]) => `O campo ${field} deve ser menor que ${formatFileSize(size)}.`,
                url: (field) => `O campo ${field} não é uma URL válida.`
            }
        }
    }
});

Vue.filter('currency', function (value) {
    if (typeof value !== 'number') {
        return value;
    }
    const formatter = new Intl.NumberFormat('pt-BR', {
        style: 'currency',
        currency: 'BRL',
        minimumFractionDigits: 2
    });
    return formatter.format(value);
});

new Vue({
    el: '#app',
    data: {
        vehicles: [],
        loading: false,
        vehicle: 0,
        pavedAmount: 0,
        unpavedAmount: 0,
        weight: 0,
        cost: 0,
        showCost: false
    },
    methods: {
        loadVehicles() {
            this.loading = true;
            request('GET', '/tcc/api/transport/vehicles').then(payload => {
                this.vehicles = JSON.parse(payload);
                this.loading = false;
            });
        },

        reset() {
            this.pavedAmount = 0;
            this.unpavedAmount = 0;
            this.weight = 0;
            this.showCost = false;
            this.cost = 0;
            this.errors.clear();
        },

        submit() {
            this.$validator.validateAll().then(result => {
                if (result) {
                    const args = `?vehicle=${this.vehicle}&weight=${this.weight}&paved=${this.pavedAmount}&unpaved=${this.unpavedAmount}`;
                    request('GET', `/tcc/api/transport/calculate/${args}`)
                        .then(payload => {
                            this.cost = parseFloat(payload);
                            this.showCost = true;
                        });
                }
            });
        }
    },
    created() {
        this.loadVehicles();
    }
});