package com.softplan.jdr.server;

import com.softplan.jdr.server.api.TransportationCostsCalculatorAPI;

import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

public class SoftplanTestApplication extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new HashSet<>();
        resources.add(TransportationCostsCalculatorAPI.class);
        return resources;
    }
}
