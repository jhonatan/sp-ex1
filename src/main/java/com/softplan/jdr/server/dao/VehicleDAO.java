package com.softplan.jdr.server.dao;

import com.softplan.jdr.server.model.Vehicle;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class VehicleDAO {

    private final Map<Integer, Vehicle> vehicles;

    public VehicleDAO() {
        this.vehicles = new HashMap<>();
        final ObjectMapper mapper = new ObjectMapper();

        // load vehicles
        Vehicle[] vehicles = null;
        try {
            vehicles = mapper.readValue(ClassLoader.getSystemResourceAsStream("vehicles.json"), Vehicle[].class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (vehicles != null) {
            for (final Vehicle v : vehicles) {
                this.vehicles.put(v.getId(), v);
            }
        }
    }

    public Vehicle getVehicleByID(final int id) {
        return vehicles.get(id);
    }

    public List<Vehicle> getVehicleList() {
        return this.vehicles.entrySet().stream()
                .map(Map.Entry::getValue)
                .sorted(Comparator.comparing(Vehicle::getId))
                .collect(Collectors.toList());
    }
}
