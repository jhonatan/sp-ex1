package com.softplan.jdr.server.model;

import com.softplan.jdr.core.RoadType;

import java.util.Map;

public class TransportationCostsCalculatorConfig {
    private int weightTreshold;
    private double exceedingWeightCost;
    private Map<RoadType, Double> roadCosts;

    public int getWeightTreshold() {
        return weightTreshold;
    }

    public void setWeightTreshold(int weightTreshold) {
        this.weightTreshold = weightTreshold;
    }

    public double getExceedingWeightCost() {
        return exceedingWeightCost;
    }

    public void setExceedingWeightCost(double exceedingWeightCost) {
        this.exceedingWeightCost = exceedingWeightCost;
    }

    public Map<RoadType, Double> getRoadCosts() {
        return roadCosts;
    }

    public void setRoadCosts(Map<RoadType, Double> roadCosts) {
        this.roadCosts = roadCosts;
    }
}