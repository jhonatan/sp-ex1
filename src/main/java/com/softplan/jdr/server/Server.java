package com.softplan.jdr.server;

import io.undertow.Undertow;
import io.undertow.server.handlers.resource.ClassPathResourceManager;
import io.undertow.server.handlers.resource.FileResourceManager;
import io.undertow.servlet.api.DeploymentInfo;
import org.jboss.resteasy.plugins.server.undertow.UndertowJaxrsServer;
import org.jboss.resteasy.spi.ResteasyDeployment;

import javax.ws.rs.core.Application;
import java.io.File;

public class Server {

    final UndertowJaxrsServer server = new UndertowJaxrsServer();

    public Server(final int port, final String host) {
        final Undertow.Builder serverBuilder = Undertow.builder().addHttpListener(port, host);
        server.start(serverBuilder);
    }

    public <T extends Application> DeploymentInfo deployApplication(final String appPath, final Class<T> appClass) {
        final ResteasyDeployment deployment = new ResteasyDeployment();
        deployment.setApplicationClass(appClass.getName());
        return server.undertowDeployment(deployment, appPath);
    }

    public void deploy(final DeploymentInfo deploymentInfo) {
        server.deploy(deploymentInfo);
    }

    public static void main(final String ... args) {
        final Server server = new Server(8080, "0.0.0.0");
        final DeploymentInfo di = server.deployApplication("/tcc", SoftplanTestApplication.class)
                .setClassLoader(Server.class.getClassLoader())
                .setContextPath("/")
                .setResourceManager(new ClassPathResourceManager(ClassLoader.getSystemClassLoader(), "public"))
                .addWelcomePage("index.html")
                .setDeploymentName("tcc.war");
        server.deploy(di);

    }
}
