package com.softplan.jdr.server.business;

import com.softplan.jdr.core.RoadType;
import com.softplan.jdr.core.TransportationCostsCalculator;
import com.softplan.jdr.server.dao.VehicleDAO;
import com.softplan.jdr.server.model.TransportationCostsCalculatorConfig;
import com.softplan.jdr.server.model.Vehicle;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public class TransportationCostsBusiness {

    private final VehicleDAO vehicleDAO;
    private final TransportationCostsCalculator tcc;

    public TransportationCostsBusiness() {
        vehicleDAO = new VehicleDAO();

        final ObjectMapper mapper = new ObjectMapper();
        final TransportationCostsCalculatorConfig config;
        try {
            config = mapper.readValue(ClassLoader.getSystemResourceAsStream("config.json"), TransportationCostsCalculatorConfig.class);
        } catch (IOException e) {
            throw new ExceptionInInitializerError(e);
        }

        tcc = new TransportationCostsCalculator();
        tcc.setExceedingWeightCost(config.getExceedingWeightCost());
        tcc.setWeightThreshold(config.getWeightTreshold());
        final Map<RoadType, Double> costs = config.getRoadCosts();
        costs.forEach((type, cost) -> tcc.setCost(cost, type));
    }

    public double calculate(final int vehicleId, final int weight, final int paved, final int unpaved) {
        final Vehicle vehicle = vehicleDAO.getVehicleByID(vehicleId);
        if (vehicle == null) {
            throw new IllegalArgumentException("Vehicle not found");
        }

        tcc.set(paved, RoadType.PAVED);
        tcc.set(unpaved, RoadType.UNPAVED);
        return tcc.calculate(weight, vehicle.getFactor());
    }

    public List<Vehicle> getVehicleList() {
        return vehicleDAO.getVehicleList();
    }
}
