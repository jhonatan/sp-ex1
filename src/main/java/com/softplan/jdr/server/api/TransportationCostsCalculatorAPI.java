package com.softplan.jdr.server.api;

import com.softplan.jdr.core.RoadType;
import com.softplan.jdr.core.TransportationCostsCalculator;
import com.softplan.jdr.server.api.TransportationCostsCalculatorAPI.Constants.Params;
import com.softplan.jdr.server.business.TransportationCostsBusiness;
import com.softplan.jdr.server.model.TransportationCostsCalculatorConfig;
import com.softplan.jdr.server.model.Vehicle;
import org.codehaus.jackson.map.ObjectMapper;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.List;
import java.util.Map;

@Path(TransportationCostsCalculatorAPI.Constants.CONTEXT)
public class TransportationCostsCalculatorAPI {

    interface Constants {
        String CONTEXT = "/api/transport";
        String VEHICLES = "/vehicles";
        String CALCULATE = "/calculate";

        interface Params {
            String VEHICLE = "vehicle";
            String WEIGHT = "weight";
            String PAVED = "paved";
            String UNPAVED = "unpaved";
        }

    }

    private final TransportationCostsBusiness business;

    public TransportationCostsCalculatorAPI() {
       business = new TransportationCostsBusiness();
    }

    @GET
    @Path(Constants.VEHICLES)
    @Produces(MediaType.APPLICATION_JSON)
    public Response listVehicles() {
        final List<Vehicle> vehicles = business.getVehicleList();
        return Response.ok(vehicles).build();
    }

    @GET
    @Path(Constants.CALCULATE)
    @Produces(MediaType.TEXT_PLAIN)
    public Response calculate(@QueryParam(Params.VEHICLE) final Integer vehicleId, @QueryParam(Params.WEIGHT) final Integer weight,
                              @QueryParam(Params.PAVED) final Integer paved, @QueryParam(Params.UNPAVED) final Integer unpaved) {


        try {
            final double cost = business.calculate(vehicleId, weight, paved, unpaved);
            return Response.ok(cost).build();
        } catch (final IllegalArgumentException e) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }
}
