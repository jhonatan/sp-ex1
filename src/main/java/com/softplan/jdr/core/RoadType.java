package com.softplan.jdr.core;

public enum RoadType {
    PAVED,
    UNPAVED
}
