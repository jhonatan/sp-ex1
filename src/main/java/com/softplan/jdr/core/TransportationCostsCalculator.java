package com.softplan.jdr.core;

/**
 * Classe responsável por calcular o custo de transporte de uma carga de acordo com o veículo
 * utilizado e a distância rodada.
 */
public class TransportationCostsCalculator {

    private final int[] distances;
    private final double[] costs;
    private int weightThreshold;
    private double exceedingWeightCost;

    public TransportationCostsCalculator() {
        final int sz = RoadType.values().length;

        this.distances = new int[sz];
        this.costs = new double[sz];
    }

    public void setWeightThreshold(final int threshold) {
        if (threshold < 0) {
            throw new IllegalArgumentException("Negative threshold is not allowed");
        }
        this.weightThreshold = threshold;
    }

    public void setExceedingWeightCost(final double cost) {
        if (cost < 0) {
            throw new IllegalArgumentException("Negative cost is not allowed");
        }
        this.exceedingWeightCost = cost;
    }


    public void setCost(final double cost, final RoadType type) {
        if (cost < 0) {
            throw new IllegalArgumentException("Negative cost is not allowed");
        }
        if (type == null) {
            throw new IllegalArgumentException("RoadType is mandatory");
        }
        costs[type.ordinal()] = cost;
    }

    /**
     * Adiciona uma distância rodada ({@code distance}) em determinado tipo de rodovia ({@code type})
     *
     * @param distance distância em km
     * @param type tipo da rodovia
     *
     * @throws IllegalArgumentException se {@code distance} for < 0
     */
    public void add(final int distance, final RoadType type) {
        if (distance < 0) {
            throw new IllegalArgumentException("Negative distance is not allowed");
        }
        if (type == null) {
            throw new IllegalArgumentException("RoadType is mandatory");
        }
        distances[type.ordinal()] += distance;
    }

    /**
     * Troca a distância rodada ({@code distance}) em determinado tipo de rodovia ({@code type})
     *
     * @param distance distância em km
     * @param type tipo da rodovia
     *
     * @throws IllegalArgumentException se {@code distance} for < 0
     */
    public void set(final int distance, final RoadType type) {
        if (distance < 0) {
            throw new IllegalArgumentException("Negative distance is not allowed");
        }
        if (type == null) {
            throw new IllegalArgumentException("RoadType is mandatory");
        }
        distances[type.ordinal()] = distance;
    }

    /**
     * Calcula o custo do transporte da carga de acordo com as especificações.
     * Para especificar um fator multiplicador de custo, utilize {@link TransportationCostsCalculator#calculate(int, double)}
     * O Valor default para o fator é 1.
     * @param weight peso da carga em toneladas
     *
     * @return um valor >= 0.0 representando o custo da carga em reais.
     *
     * @throws IllegalArgumentException se {@code weight} for < 0
     */
    public double calculate(final int weight) {
        return calculate(weight, 1.0);
    }

    /**
     * Calcula o custo do transporte da carga de acordo com as especificações
     *
     * @param weight peso da carga em toneladas
     * @param factor fator multiplicador do custo
     *
     * @return um valor >= 0.0 representando o custo da carga em reais.
     *
     * @throws IllegalArgumentException se {@code weight} for < 0
     * @throws IllegalArgumentException se {@code factor} for < 0
     */
    public double calculate(final int weight, final double factor) {
        if (weight < 0) {
            throw new IllegalArgumentException("Negative weight is not allowed");
        }

        if (factor < 0) {
            throw new IllegalArgumentException("Negative factor is not allowed");
        }

        double exceedingCostByDistance = 0;
        if (weight > weightThreshold) {
            exceedingCostByDistance = (weight - weightThreshold) * exceedingWeightCost;
        }

        double cost = 0;
        for(int i = 0; i < distances.length; i++) {
            final int d = distances[i];
            cost += d * (costs[i]) * factor + d * exceedingCostByDistance;
        }

        // round to two decimal places
        return Math.round(cost * 100)/100.0;
    }
}
