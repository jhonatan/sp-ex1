Softplan Interview Test - Ex. 1
---
Calculadora de custos de transporte.

Passos para compilar e executar o servidor:
1. `mvn install` (irá gerar o arquivo `target/jdr.jar`)
2. `java -jar target/jdr.jar`

O servidor sobe em `http://localhost:8080`